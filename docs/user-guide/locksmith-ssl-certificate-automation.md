# SSL Certificate Automation for Locksmith

Duke OKD includes a template that can be used to automate provisioning of SSL/TLS certificates from Duke's [Locksmith](https://locksmith.oit.duke.edu/) service using Locksmith's [ACME Protocol integration](https://locksmith.oit.duke.edu/help/acme).

!!! warning
    The SSL Certificate Automation service in Duke OKD only support *.duke.edu domains.  If you have a non-Duke domain, you will need follow the normal procedure for generating and signing certificates.

!!! note
    `*.cloud.duke.edu` domains do not need a custom SSL Certificate.  Duke OKD already has a wildcard certificate in place for these domains.


## Enabling Certificate Automation in the Web UI

From the Duke OKD service catalog or the `Browse Catalog` link in your project, select the `SSL Cert Automation` service.  This will open a form asking for a `*@duke.edu` email address to associate future certificates with.  Pick a valid `*@duke.edu` email address, and click next.

!!! note
    It is good practice to use a group email address for the registration, rather than an individual's email address.

On the next page, select `Create a secret in <your project> to be used later`, and click Create.

Next go into your project, and select the route you wish to have `openshift-acme` manage.  Click the `Actions` button, and select `Edit YAML`.

In the YAML description of your route, add the annotation `kubernetes.io/tls-acme: true`, like below, and save your changes.

```
apiVersion: route.openshift.io/v1
kind: Route
metadata:
  annotations:
    kubernetes.io/tls-acme: 'true'
```

`openshift-acme` will then register your SSL certificate and update your route.  In normal operation, this process takes a few minutes, but could be longer depending on how quickly the certificate is signed by Duke's parent certificate authority.


## Enabling Certificate Automation using the `oc` cli command

Within your project, use the `oc new-app` command to deploy `openshift-acme` from the template.  Make sure to pass the `EMAIL_ADDRESS` parameter, using the address you would like Locksmith to associate with your certificates.

!!! note
    It is good practice to use a group email address for the registration, rather than an individual's email address.

```
$ oc new-app openshift-acme --param=EMAIL_ADDRESS=your-email@duke.edu
```

When the `openshift-acme` deployment is running, annotate your route with the `kubernetes.io/tls-acme: true` annotation:

```
$ oc annotate route go-hello kubernetes.io/tls-acme=true
```

`openshift-acme` will then register your SSL certificate and update your route.  In normal operation, this process takes a few minutes, but could be longer depending on how quickly the certificate is signed by Duke's parent certificate authority.


## How it works

The `SSL Cert Automation` service available in the [Duke OKD Service Catalog](https://manage.cloud.duke.edu/console/catalog) creates a deploymentConfig named `openshift-acme`.  The deploymentConfig consists of a single pod, which watches the routes in your project, and if an annotation is found, attempts to validate your ownership of the domain and request SSL certificates from Locksmith.


### How does OpenShift-ACME know which routes to manage?

When `openshift-acme` is deployed, it will attempt to validate, register certs, and update TLS settings for any routes that have the annotation `kubernetes.io/tls-acme=true`.  `openshift-acme` will also watch for new routes, or new annotations on existing routes, and manage them as well.


### Domain Ownership Validation

For any routes that it's managing, `openshift-acme` will create a secondary route to itself with an auto-generated secret to allow Locksmith to validate your ownership of the domain using the ACME protocol.  For this to work, the CNAME or A Record for the [domain must already be pointed to the Duke OKD routers](/cluster-details/dns/).

!!! warning
    In order for Locksmith to be able to read the secret from the route, you must enable insecure traffic (ie: HTTP traffic) for your existing route.  If your route is already configured for HTTPS, does not use a valid certificate and does not allow HTTP traffic, the ACME domain ownership validation will fail.  This problem will manifest as constant `Re-queuing Route "your-route" due to pending authorization` errors in the `openshift-acme` pod logs.


### SSL Certificate Deployment

Once `openshift-acme` has validated the domain, registered a certificate request with Locksmith, and received a signed certificate, it automatically updates the route with the SSL information and enables secure traffic.

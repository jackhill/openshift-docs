# Trigger Builds from changes to Dockerhub Images

While triggering builds from webhook and image stream changes is relatively well documented in the [OKD/OKD Documentation](https://docs.okd.io/latest/dev_guide/builds/triggering_builds.html#image-change-triggers), it isn't quite as clear how to setup a trigger based on changes to an upstream Dockerhub image.  Despite the lack of clear documentation, the process is easy, however.

Steps involved:

1.  Create an image stream to watch the upstream image
2.  Create an image change trigger to watch the new image stream

## Create an image stream to watch the upstream image

The documentation generally describes creating an image stream object to track *all* of the upstream image tags from an upstream image, but this is not desirable if you're using a single tag.  In addition, it is generally good practice to schedule OKD/OKD to watch the upstream image for changes, and pull the new image to the image stream when it is updated.

The process of creating an image from an external registry, like Dockerhub, is called "importing" the image.  All this does, though, is create an image stream that points to the external registry.

In order to import a single image tag (in this example, centos:centos7), the following can be run from the command line:

`oc import-image docker.io/library/centos:centos7 --confirm --scheduled=true`

The `--scheduled=true` argument tells OKD to periodically scan the upstream image for changes and import them when it finds them.

Alternatively, the image stream object can be defined as yaml and created directly with `oc create -f <yaml file>`:

```
apiVersion: "v1"
kind: "ImageStream"
metadata:
  name: "centos"
spec:
  lookupPolicy:
    local: false
  tags:
  - annotations: null
    from:
      kind: DockerImage
      name: docker.io/library/centos:centos7
    generation: 1
    importPolicy:
      scheduled: true
    name: centos7
    referencePolicy:
      type: Source
```

## Create an image change trigger to watch the new image stream

Once the project has an image stream tracking the upstream image that will be used for a build, the build config can be adjusted to start a new build when that image stream changes (ie, a new image is pushed to that image tag).

In the build config, make sure the "from" field for the build strategy references the image stream using the kind `ImageStreamTag`.  For example, a "Docker" strategy might look like as follows:

```
spec:
  strategy:
  type: Docker
  dockerStrategy:
    from:
      kind: "ImageStreamTag"
      name: "centos:centos7"
```

!!! note
    For the purposes of brevity, the .spec above contains only the pieces referencing the build strategy.  See the [Build Config Documentation](https://docs.okd.io/latest/dev_guide/builds/index.html#defining-a-buildconfig) for examples of a complete build config.

In addition to watching the Image Stream Tag that points to the upstream image, the build config also needs a trigger to start a new build when that image changes.  With the same spec, add the trigger:

```
spec:
  strategy:
  type: Docker
  dockerStrategy:
    from:
      kind: "ImageStreamTag"
      name: "centos:centos7"
  triggers:
  - type: imageChange
    imageChange: {}
```

Now, in this example, the build config is setup to start a new build whenever OKD/OKD polls Dockerhub and finds a new image has been pushed to the "centos:centos7" image tag.

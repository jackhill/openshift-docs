# Resource Requests, Limits and Quotas

Duke OKD applies a default quota to resources on a project-level basis.  New projects receive a default quota when they are created.  The quota can be increased by request to the cluster administrators.


## What are Resources, Requests, Limits and Quotas?

What are Resources, and how are they managed by Requests, Limits and Quotas?


### Resources

Resources within Duke OKD describe a number of items.  Examples include:

*   CPU allowed for the project
*   memory allowed for the project
*   number of pods in the project
*   number of configMaps in the project

!!! info "How do CPU resources work?"
    The CPU resource is measured in CPU units. One CPU, in Duke OKD, is equivalent to 1 hyperthread of a VMWare vCPU with hyperthreading.  In laypeople's terms: 1 core.


Per the [Kubernetes Documentation on CPU resources](https://kubernetes.io/docs/tasks/configure-pod-container/assign-cpu-resource/):

> "Fractional values are allowed. A Container that requests 0.5 CPU is guaranteed half as much CPU as a Container that requests 1 CPU. You can use the suffix m to mean milli. For example 100m CPU, 100 milliCPU, and 0.1 CPU are all the same. Precision finer than 1m is not allowed.  CPU is always requested as an absolute quantity, never as a relative quantity; 0.1 is the same amount of CPU on a single-core, dual-core, or 48-core machine."


### Quotas

A `quota` sets hard maximums for the resources on the project level.  Quotas in a given project can be view with the `oc get quotas` command, or in the Web UI under "Resources -> Quota" within the project.

As of April 2019, the default quota applied to all new projects looks like so:

    $ oc describe quota default-quota
    Name:                   default-quota
    Namespace:              test-project
    Resource                Used   Hard
    --------                ----   ----
    configmaps              0      10
    limits.cpu              200m   2
    limits.memory           200Mi  2Gi
    persistentvolumeclaims  0      6
    pods                    1      6
    replicationcontrollers  0      20
    requests.cpu            100m   1
    requests.memory         20Mi   1Gi
    secrets                 14     20
    services                1      10

As an example, the output above shows a maximum of 10 configMaps for the project (with 0 currently in use).


### Limits

Limits are resources constraints at the pod, container, image, image stream, and persistent volume claim level and define the maximum resources those items can consume.  A default limit range is set on each new project by the cluster administrators.  Limits can be seen using the `oc get limits` command.

As of April 2019, for example, the `default-limit-range` set for projects looks like so:

    $ oc describe limits default-limit-range
    Name:       default-limit-range
    Namespace:  test-project
    Type        Resource  Min  Max  Default Request  Default Limit  Max Limit/Request Ratio
    ----        --------  ---  ---  ---------------  -------------  -----------------------
    Container   memory    -    -    20Mi             200Mi          -
    Container   cpu       -    -    100m             200m           -

As you can see containers in projects are limited to 200Mi memory usage and 200m CPU by default (`Default Limit`).


### Requests

Requests define the _minimum_ amount of a resource that your pods/containers/etc need to perform their function.  Requests allow OKD/Kubernetes to place your pods on nodes with the appropriate amount of resources available.

!!! warning "Requests are REQUIRED"
    Requests are required when creating container objects in Duke OKD.  However, the cluster admins have set a default request size for any new container calls that do not have a request included.  Viewing the `default-limit-range`, you can see the Default Requests set by Duke OKD when new containers are created, unless they are otherwise specified.


### Setting Limits and Requests

Unlike quotas, which are managed by cluster administrators, developers can (and are encouraged to) set limits and requests for the objects they create within OKD.  Setting limits can ensure that no single pod or containers uses up all the resources allowed to your project by the quota set for it.  Setting requests can ensure that Kubernetes assigns your pods to nodes with enough resources to handle the overhead without killing and migrating your pods to another node.

More information is available in the:

*   [Kubernetes Requests, Limits and Quota Documentation](https://kubernetes.io/docs/concepts/policy/resource-quotas)
*   [OKD Requests, Limits and Quota  Documentation](https://docs.openshift.com/container-platform/3.11/dev_guide/compute_resources.html)


## Requesting a quota increase

For now, requests for quota increase should be handled by submitting a ticket to the [OIT Service Desk](https://oit.duke.edu/help), and providing:

*   The project in which to increase the quota
*   The resource (eg. CPU shares, memory, configMaps, etc) to be increased
*   The value to be increased to (eg. CPU shares to 300m, memory to 3Gi, configMaps to 20)

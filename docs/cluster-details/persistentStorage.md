# Persistent Storage Options

_Updated: 2019-02-18_

Duke OKD/OKD supports persistent storage though [Kubernets Storage Classes](https://kubernetes.io/docs/concepts/storage/storage-classes/) and provisions persistent storage for your applications dynamically when you create a [Persistent Volume Claim](https://kubernetes.io/docs/concepts/storage/persistent-volumes/#persistentvolumeclaims).

_Requesting Persistent Volumes via Persistent Volume Claims is outside the scope of this document.  The [Kubernetes Persistent Volume Documentation](https://kubernetes.io/docs/concepts/storage/persistent-volumes/) is a good place to start for that._


## Supported Storage Classes

Currently, Duke OKD has two Storage Classes available:

1.  standard (default)
2.  glusterfs-storage-block

You can see the available Storage Classes with the `oc get storageclasses` command.


## Which storage class is right for me?

_standard_

Generally, the standard storage class should work for most applications.  This storage class option is the default storage class, and will be selected automatically if you do not specify a storage class when creating a Persistent Volume Claim.

The Standard storage class is a shared filesystem, so multiple pods can write to it at once.

_glusterfs-storage-block_

The glusterfs-storage-block storage class is block storage that allows you to mount a block device into a single pod.  Block storage is needed for applications that do not handle multiple writes well.  _It is unlikely that you need this storage type._

ElasticSearch, for example, does not generally handle multiple-write operations to the same filesystem very well.  Specifically, there is a known issue with ElasticSearch and the standard storage class which causes data corruption almost immediately, and gluster-storage-block should be used instead.

The glusterfs-storage-block storage class is a _manually provisioned_ class, meaning PVCs are not automatically bound to persistent volumes on creation.  Requests for block storage must be approved by the cluster administrators.  If you would like a block volume, please email openshift-team@duke.edu for assistance.

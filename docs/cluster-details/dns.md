# Host Names and Duke DNS integration

_Updated: 2019-02-18_

## cloud.duke.edu

Duke OKD has an assigned wild-card DNS entry for `*.cloud.duke.edu`, so any traffic to a host name in the cloud.duke.edu zone that is not explicitly reserved in DNS will be directed to the OKD routers and handled by OKD.

__What this means for you as a user of OKD:__

When you create a [route](https://docs.okd.io/latest/dev_guide/routes.html) to your service within OKD, you have the opportunity to add a host name, or allow OKD to generate one.  Generated host names will be placed in the cloud.duke.edu zone, and DNS entries will be automatically created and handled by OKD, so you will not need to do anything else.

If you specify a host name manually, _and it is within the cloud.duke.edu zone_, OKD will give you that DNS entry as long as it doesn't already exist.  It will automatically be handled, and you will not need to do anything else.

__HTTPS/TLS Considerations__

Anything within the cloud.duke.edu zone will be able to make use of the wild-card TLS certificate for encrypted HTTPS traffic.

See [Using TLS Certificates with OKD](/cluster-details/tls-certificates) for more info.


## Non-cloud.duke.edu

If you would like a DNS entry that is not within the `cloud.duke.edu` zone, for example, something.oit.duke.edu, or even a non-Duke DNS entry, you can create the route as you would normally, and specify the host name you want to use.

[As long as the host name has been approved for use](https://oit.duke.edu/what-we-do/services/domain-name-system-dns), you can submit a ticket to the [OIT Service Desk](https://oit.duke.edu/help) requesting that a CNAME for your name of choice be directed to the OKD routers: `os-node-lb-fitz.oit.duke.edu`

The request will be evaluated by the team responsible for DNS within OIT, and barring any issues, setup as desired.

__HTTPS/TLS Considerations__

Routes using host names in zones other than cloud.duke.edu will need to provide their own TLS certificates for HTTPS/TLS encrypted traffic.

See [Using TLS Certificates with OKD](/cluster-details/tls-certificates) for more info.

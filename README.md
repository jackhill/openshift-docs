# Duke University OKD Documentation

Duke University OKD: A managed platform for developing and deploying webapps in your choice of programming languages, versions and frameworks - on-demand.

* Development Environment in Minutes
* Choice of Languages, Tools, and Frameworks
* Centrally Managed Infrastructure


## <a name="features"></a>Features and Benefits

**Focus On Development:** Worry about your application rather than infrastructure management.  We'll manage the servers - you can focus on the code.

**On-Demand Environment:** Click the Subscribe link above to start provisioning your development container. Once subscribed, you can create development environments in just a few minutes.

**Choice of Languages:** Choose from a list of popular programming languages and frameworks. Numerous community-supported starter code will get you going with other software options.

**Enterprise Infrastructure:** The service is hosted on server class hardware and is maintained by OIT personnel whom already provide enterprise-level management and support to critical university software applications.


## <a name="what_is_okd"></a> What is OKD?

[OKD](https://okd.io) (formerly OpenShift Origin) is a distribution of [Kubernetes](https://kubernetes.io) optimized for continuous application development to enable rapid application development, easy deployment and scaling.


## <a name="getting_started"></a> Getting Started

The Duke OKD project is currently an open Beta program.  You can create a new project with the [OIT Cloud Registrar](https://cloud.duke.edu).  A Duke FundCode is required.  Non-OIT projects will be supported best-effort until the end of the Beta program.

Then, go to the [OKD Management Console](https://manage.cloud.duke.edu) to manage your projects..


## <a name="doug"></a> Duke OpenShift Users Group

The Duke OpenShift Users Group (or DOUG) is a community group open to all Duke University and Duke Health folks interested in learning and sharing with other OKD/OpenShift users at Duke.

The group consists of:

*  a mailing list (openshift-users@duke.edu) to notify members of future group meetings.
*  a microsoft teams team [Duke Openshift Users Group Team](https://teams.microsoft.com/l/team/19%3a8fc117cd5cf14856860c1b366507ad95%40thread.skype/conversations?groupId=f66622b8-ed9e-4fa4-927e-137089e86a0f&tenantId=cb72c54e-4a31-4d9e-b14a-1ea36dfac94c) where members can communicate with each other in real time
*  Repositories in the [Duke Openshift Users Gitlab Group](https://gitlab.oit.duke.edu/duke_openshift_users)

Members of the Group should read the [Code of Conduct](https://gitlab.oit.duke.edu/duke_openshift_users/information/blob/master/CodeOfConduct.md).


## <a name="okd-office-hours"></a> OKD Office Hours

OIT Hosts weekly "OKD Office Hours" in-person on Tuesdays at 1pm at the OIT Office at the American Tobacco Campus, or via Zoom teleconference.  Contact Chris Collins (christopher.collins@duke.edu) for the Zoom meeting details.
